package com.shenll.ticketcreator.Model;

/**
 * Created by SH-PC-W8.1-3 on 7/14/2017.
 */

public class Project {
    public String name;
    public String assignee;


    public Project() {
    }

    public Project(String name, String assignee) {

        this.name = name;
        this.assignee = assignee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
