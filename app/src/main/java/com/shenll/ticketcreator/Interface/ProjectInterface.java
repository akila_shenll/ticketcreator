package com.shenll.ticketcreator.Interface;

import android.view.View;

import com.shenll.ticketcreator.Model.Project;


/**
 * Created by SH-PC-W8.1-3 on 7/11/2017.
 */

public interface ProjectInterface {
    void onProjectClickListener(View convertView, int position, Project project);
}
