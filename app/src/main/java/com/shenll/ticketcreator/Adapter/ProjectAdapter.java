package com.shenll.ticketcreator.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shenll.ticketcreator.Interface.ProjectInterface;
import com.shenll.ticketcreator.Model.Project;
import com.shenll.ticketcreator.R;

import java.util.List;

/**
 * Created by SH-PC-W8.1-3 on 7/13/2017.
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.MyViewHolder> {
    private Context context;
    private List<Project> projectList;
    ProjectInterface projectInterface;

    public ProjectAdapter(Context context, List<Project> projectList) {
        this.context = context;
        this.projectList = projectList;
    }

    public void setOnClickListener(ProjectInterface projectInterface) {
        this.projectInterface = projectInterface;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, assignee;
        public CardView task_card_view;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.show_ip_name);
            assignee = (TextView) itemView.findViewById(R.id.show_ip_assignee);
            task_card_view = (CardView) itemView.findViewById(R.id.pro_card_view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final ProjectAdapter.MyViewHolder holder, final int position) {
        final Project project = projectList.get(position);
        holder.name.setText(project.getName());
        holder.assignee.setText(project.getAssignee());

        holder.task_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectInterface.onProjectClickListener(view, position, project);
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectList.size();
    }

}
