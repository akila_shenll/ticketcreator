package com.shenll.ticketcreator.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.shenll.ticketcreator.Activity.MainActivity;
import com.shenll.ticketcreator.R;

/**
 * Created by SH-PC-W8.1-3 on 7/12/2017.
 */

public class SplashActivity extends Activity {

    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            // Showing splash screen with a timer.Showing app logo / company

            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
