package com.shenll.ticketcreator.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shenll.ticketcreator.Adapter.ProjectAdapter;
import com.shenll.ticketcreator.Interface.ProjectInterface;
import com.shenll.ticketcreator.Model.Project;
import com.shenll.ticketcreator.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private String projectID;
    RecyclerView recyclerView;
    List<Project> projectList = null;
    CardView card_view;
    FloatingActionButton add_fab;

    Button ok;
    EditText input_name, input_assignee;
    TextInputLayout layout_input_name, layout_input_assignee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        card_view = (CardView) findViewById(R.id.card_view);
        add_fab = (FloatingActionButton) findViewById(R.id.add_fab);
        ok = (Button) findViewById(R.id.btn_ok);

        projectList = new ArrayList<Project>();

        layout_input_name = (TextInputLayout) findViewById(R.id.layout_input_name);
        layout_input_assignee = (TextInputLayout) findViewById(R.id.layout_input_assignee);

        input_name = (EditText) findViewById(R.id.input_name);
        input_assignee = (EditText) findViewById(R.id.input_assignee);


        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        if (mFirebaseDatabase == null) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
            mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("Projects");
            mFirebaseDatabase.keepSynced(true);
        }

        setProjectList();

        ok.setOnClickListener(this);
        add_fab.setOnClickListener(this);

    }

    public void setProjectList() {
        if (projectList != null) {
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);

            ProjectAdapter projectAdapter = new ProjectAdapter(this, projectList);

            recyclerView.setAdapter(projectAdapter);

            projectAdapter.setOnClickListener(new ProjectInterface() {
                @Override
                public void onProjectClickListener(View convertView, int position, Project project) {

                    Intent intent = new Intent(MainActivity.this, TaskActivity.class);
                    startActivity(intent);
                    //finish();
                }
            });
        } else {

            Toast.makeText(this, "No Projects Results", Toast.LENGTH_SHORT).show();
        }
    }

    private void createProject(String name, String assignee) {

        if (TextUtils.isEmpty(projectID)) {
            projectID = mFirebaseDatabase.push().getKey();
        }

        Project projectDb = new Project(name, assignee);

        mFirebaseDatabase.child(projectID).setValue(projectDb);
        addUserChangeListener();

    }

    public void addUserChangeListener() {
        mFirebaseDatabase.child(projectID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Project project = dataSnapshot.getValue(Project.class);
                Log.e(TAG, "User data is changed!" + project.name + ", " + project.assignee);
                input_name.setText("");
                input_assignee.setText("");


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
                System.out.println("Firebase Error Code" + error.getCode());
            }
        });

        mFirebaseDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Project project = dataSnapshot.getValue(Project.class);

                project.setName("Project Name :" + project.name);
                project.setAssignee("Assignee :" + project.assignee);

                projectList.add(project);

                setProjectList();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                add_fab.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
                card_view.setVisibility(View.GONE);
                String name = input_name.getText().toString();
                String assignee = input_assignee.getText().toString();
                if (name.isEmpty() || assignee.isEmpty()) {
                    Toast.makeText(this, "Values are Empty", Toast.LENGTH_SHORT).show();
                } else {
                    createProject(name, assignee);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                input_name.setText("");
                input_assignee.setText("");
                break;
            case R.id.add_fab:
                add_fab.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                card_view.setVisibility(View.VISIBLE);

                break;
        }
    }
}
