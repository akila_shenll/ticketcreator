package com.shenll.ticketcreator;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.shenll.ticketcreator.Activity.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static java.lang.Thread.sleep;

/**
 * Created by SH-PC-W8.1-3 on 7/13/2017.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mainActivity = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testMainActivity() {
        try {
            sleep(10000);
            onView(withId(R.id.add_fab)).perform(click());
            onView(withId(R.id.input_name)).perform(click()).check(matches(isDisplayed()));
            onView(withId(R.id.input_assignee)).perform(click()).check(matches(isDisplayed()));
            onView(withId(R.id.btn_ok)).perform(click());

            sleep(5000);
            onView(withId(R.id.show_ip_name)).check(matches(isDisplayed()));
            onView(withId(R.id.show_ip_assignee)).check(matches(isDisplayed()));
            onView(withId(R.id.pro_card_view)).check(matches(isDisplayed()));

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @After
    public void tearDown() throws Exception {

    }

}