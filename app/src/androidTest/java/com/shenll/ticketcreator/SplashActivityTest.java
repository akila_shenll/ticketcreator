package com.shenll.ticketcreator;

import android.graphics.Point;
import android.os.RemoteException;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import com.shenll.ticketcreator.Activity.SplashActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Created by SH-PC-W8.1-3 on 7/12/2017.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class SplashActivityTest {
    @Rule
    public ActivityTestRule<SplashActivity> splash = new ActivityTestRule(SplashActivity.class, true  , true );

    @Before
    public void setUp() throws Exception {

            UiDevice uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            Point[] coordinates = new Point[4];
            coordinates[0] = new Point(248, 1520);
            coordinates[1] = new Point(248, 929);
            coordinates[2] = new Point(796, 1520);
            coordinates[3] = new Point(796, 929);
            try {
                if (!uiDevice.isScreenOn()) {
                    uiDevice.wakeUp();
                    uiDevice.swipe(coordinates, 10);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
    }

    @Test
    public void testLaunch() {
        onView(withId(R.id.splash_image)).check(matches(isDisplayed()));
    }
    @Test
    public void testActivityClosed(){
        try {
            Thread.sleep(2000);
            assertTrue(!splash.getActivity().isFinishing());
            Thread.sleep(3000);
            assertTrue(splash.getActivity().isFinishing());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception {

    }

}